package fr.unice.polytech.gitolite.access;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by foerster
 * on 12/06/14.
 */
@XmlRootElement
public class DummyClasse {
    public List<String> dummies ;
    public DummyClasse() {}

    public DummyClasse(List<String> dummies){
        this.dummies = dummies;
    }
}
